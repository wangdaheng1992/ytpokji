FROM openjdk:8-jre-alpine
MAINTAINER wang daheng "dhwang@thoughtworks.com"
COPY /build/libs/ytpokji-1.0-SNAPSHOT.war /app/ytpokji-1.0-SNAPSHOT.war
ENTRYPOINT ["java"]
CMD ["-jar", "./app/ytpokji-1.0-SNAPSHOT.war"]
EXPOSE 8888
