#!/usr/bin/env bash

CONTAINER_NAME=ytpokji-mysql
if [ ! "$(docker ps -q -f name=$CONTAINER_NAME)"]; then
    echo "mysql is not running or not exist"
    if [ "$(docker ps -aq -f name=$CONTAINER_NAME)"]; then
        echo "delete not working mysql docker"
        docker rm $CONTAINER_NAME
    fi
    echo "run mysql docker"
    docker run -d --restart always --name $CONTAINER_NAME \
        -p 3310:3306 -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=ytpokji \
            mysql:5.6.40 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci

    sleep 30s
fi

