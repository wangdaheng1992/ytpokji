CREATE TABLE blog_image (
  id     VARCHAR(36) PRIMARY KEY,
  name   VARCHAR(255) NOT NULL,
  type   VARCHAR(36)         NOT NULL,
  length BIGINT       NOT NULL,
  url    VARCHAR(255) NOT NULL
);