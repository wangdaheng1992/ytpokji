CREATE TABLE schedule(
  id VARCHAR(36) PRIMARY KEY,
  name VARCHAR(36) not NULL,
  duration BIGINT NOT NULL,
  description VARCHAR(255) NOT NULL,
  start_date DATE NOT NULL,
  end_date DATE NOT NULL
);
