
CREATE TABLE blog(
  id VARCHAR(36) PRIMARY KEY,
  title VARCHAR(255) not NULL,
  content TEXT NOT NULL,
  create_time DATE NOT NULL ,
  user_id VARCHAR(36) NOT NULL,
  update_time DATE
);

