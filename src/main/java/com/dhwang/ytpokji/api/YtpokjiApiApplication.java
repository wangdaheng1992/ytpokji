package com.dhwang.ytpokji.api;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YtpokjiApiApplication {

    public static void main(String[] args) {
        System.out.println("Application begin to start!");
        SpringApplication.run(YtpokjiApiApplication.class, args);
    }
}
