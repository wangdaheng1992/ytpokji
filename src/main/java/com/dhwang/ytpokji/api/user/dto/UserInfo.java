package com.dhwang.ytpokji.api.user.dto;

import com.dhwang.ytpokji.api.user.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {

    private String id;

    private String userName;

    private String name;

    public UserInfo(User user) {
        new ModelMapper().map(user, this);
    }
}
