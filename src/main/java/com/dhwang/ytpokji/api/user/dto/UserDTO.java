package com.dhwang.ytpokji.api.user.dto;

import com.dhwang.ytpokji.api.user.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

    private String userName;
    private String password;

    public UserDTO(User user) {
        new ModelMapper().map(user, this);
    }
}
