package com.dhwang.ytpokji.api.user.repository;

import com.dhwang.ytpokji.api.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {

    User findByUserNameAndPassword(String userName, String password);
}
