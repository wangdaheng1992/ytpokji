package com.dhwang.ytpokji.api.user.service;

import com.dhwang.ytpokji.api.user.dto.UserInfo;
import com.dhwang.ytpokji.api.user.entity.User;
import com.dhwang.ytpokji.api.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public UserInfo getUserInfo(String userName, String password) {
        User user = userRepository.findByUserNameAndPassword(userName, password);
        return new UserInfo(user);
    }

    public User getUserById(String userId){
        return userRepository.findOne(userId);
    }
}
