package com.dhwang.ytpokji.api.user.controler;

import com.dhwang.ytpokji.api.user.dto.UserDTO;
import com.dhwang.ytpokji.api.user.dto.UserInfo;
import com.dhwang.ytpokji.api.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/api/users")
    public UserInfo getUserInfo(@RequestBody UserDTO userDTO){
        log.info("-----userDTO value is {} ------", userDTO);
        return userService.getUserInfo(userDTO.getUserName(), userDTO.getPassword());
    }
}
