package com.dhwang.ytpokji.api;

import com.dhwang.ytpokji.api.exception.BusinessException;
import com.dhwang.ytpokji.api.exception.TechnicException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ResponseBody
@ControllerAdvice
public class DefaultExceptionHandler {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = TechnicException.class)
    public String handleTechinicException(Exception e) {
        log.error("TechinicException:", e);
        return e.getMessage();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = BusinessException.class)
    public String handleBusinessException(Exception e) {
        log.error("BusinessException:", e);
        return e.getMessage();
    }
}
