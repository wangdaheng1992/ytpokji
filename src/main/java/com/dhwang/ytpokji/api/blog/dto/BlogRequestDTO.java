package com.dhwang.ytpokji.api.blog.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogRequestDTO {

    private String title;

    private String content;

    private String imageId;

    private Date createTime;

    private String type;
}
