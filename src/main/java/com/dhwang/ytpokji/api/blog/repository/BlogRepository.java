package com.dhwang.ytpokji.api.blog.repository;


import com.dhwang.ytpokji.api.blog.entity.Blog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BlogRepository extends JpaRepository<Blog,String>{
}
