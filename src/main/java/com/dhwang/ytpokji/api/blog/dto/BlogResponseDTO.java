package com.dhwang.ytpokji.api.blog.dto;

import com.dhwang.ytpokji.api.blog.entity.Blog;
import com.dhwang.ytpokji.api.file.entity.BlogImage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogResponseDTO {

    private String id;

    private String content;

    private String title;

    private String createTime;

    private String createUser;

    private String imageUrl;

    public BlogResponseDTO(Blog blog, BlogImage image) {
        new ModelMapper().map(blog, this);
        this.imageUrl = image.getUrl();
    }
}
