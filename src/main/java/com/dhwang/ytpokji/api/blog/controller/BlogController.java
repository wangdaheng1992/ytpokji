package com.dhwang.ytpokji.api.blog.controller;

import com.dhwang.ytpokji.api.blog.dto.BlogRequestDTO;
import com.dhwang.ytpokji.api.blog.dto.BlogResponseDTO;
import com.dhwang.ytpokji.api.blog.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/blogs")
public class BlogController {

    @Autowired
    private BlogService blogService;

    @GetMapping
    public List<BlogResponseDTO> getBlogs() {
        return blogService.getAllBlogs();
    }

    @GetMapping("/{id}")
    public BlogResponseDTO getBlog(@PathVariable("id") String blogId){
        return blogService.getBlogById(blogId);
    }

    @PostMapping
    public void addBlog(@RequestBody BlogRequestDTO blogRequestDTO) {
        blogService.addBlog(blogRequestDTO);
    }
}
