package com.dhwang.ytpokji.api.blog.service;

import com.dhwang.ytpokji.api.blog.dto.BlogRequestDTO;
import com.dhwang.ytpokji.api.blog.dto.BlogResponseDTO;
import com.dhwang.ytpokji.api.blog.entity.Blog;
import com.dhwang.ytpokji.api.blog.repository.BlogRepository;
import com.dhwang.ytpokji.api.file.entity.BlogImage;
import com.dhwang.ytpokji.api.file.repository.BlogImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BlogService {

    @Autowired
    private BlogRepository blogRepository;

    @Autowired
    private BlogImageRepository blogImageRepository;

    public List<BlogResponseDTO> getAllBlogs() {
        return blogRepository.findAll().stream().map(blog -> new BlogResponseDTO(blog, blogImageRepository.findOne(blog.getImageId())))
                .collect(Collectors.toList());
    }

    public BlogResponseDTO getBlogById(String blogId) {
        Blog blog = blogRepository.findOne(blogId);
        BlogImage blogImage = blogImageRepository.findOne(blog.getImageId());
        return new BlogResponseDTO(blog, blogImage);
    }

    public void addBlog(BlogRequestDTO blogRequestDTO) {
        blogRepository.save(new Blog(blogRequestDTO));
    }
}
