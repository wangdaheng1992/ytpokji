package com.dhwang.ytpokji.api.blog.entity;

import com.dhwang.ytpokji.api.UserUtils;
import com.dhwang.ytpokji.api.blog.dto.BlogRequestDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.modelmapper.ModelMapper;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "blog")
@Entity
public class Blog {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "title")
    private String title;

    @Column(name = "content", columnDefinition = "TEXT")
    private String content;

    @CreationTimestamp
    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "image_id")
    private String imageId;

    @Column(name = "type")
    private String type;

    public Blog(BlogRequestDTO blogRequestDTO) {
        this.id = UUID.randomUUID().toString();
        this.userId = UserUtils.getUserId();
        new ModelMapper().map(blogRequestDTO, this);
    }
}
