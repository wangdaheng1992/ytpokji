package com.dhwang.ytpokji.api.schedule.repository;


import com.dhwang.ytpokji.api.schedule.entity.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScheduleRepository extends JpaRepository <Schedule, String>{
}
