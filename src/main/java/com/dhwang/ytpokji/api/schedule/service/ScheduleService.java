package com.dhwang.ytpokji.api.schedule.service;

import com.dhwang.ytpokji.api.schedule.entity.Schedule;
import com.dhwang.ytpokji.api.schedule.repository.ScheduleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ScheduleService {

    @Autowired
    private ScheduleRepository scheduleRepository;

    public List<Schedule> getSchedules() {
        List<Schedule> schedules = scheduleRepository.findAll();
        log.info("get schedule list, list is {}", schedules);
        return  schedules;
    }

    public void addSchedule(Schedule schedule) {
        log.info("Add schedule,and the request param is {}", schedule);
        scheduleRepository.save(schedule);
    }
}
