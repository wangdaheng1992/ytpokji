package com.dhwang.ytpokji.api.schedule.controller;

import com.dhwang.ytpokji.api.schedule.entity.Schedule;
import com.dhwang.ytpokji.api.schedule.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/schedule")
public class ScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    @GetMapping
    public List<Schedule> getSchedules(){
        return scheduleService.getSchedules();
    }

    @PostMapping
    public void addSchedule(@RequestBody Schedule schedule){
        scheduleService.addSchedule(schedule);
    }
}
