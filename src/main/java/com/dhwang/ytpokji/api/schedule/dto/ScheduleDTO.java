package com.dhwang.ytpokji.api.schedule.dto;

import com.dhwang.ytpokji.api.schedule.entity.Schedule;
import lombok.Data;

import java.util.Date;

@Data
public class ScheduleDTO {

    private String id;

    private String name;

    private Long duration;

    private String description;

    private Date startDate;

    private Date endDate;


    public ScheduleDTO(Schedule schedule) {

    }
}
