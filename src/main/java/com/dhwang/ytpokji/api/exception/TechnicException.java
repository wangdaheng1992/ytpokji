package com.dhwang.ytpokji.api.exception;

public class TechnicException extends RuntimeException{

    public TechnicException(String message) {
        super(message);
    }
}
