package com.dhwang.ytpokji.api.filter;

import com.dhwang.ytpokji.api.exception.BusinessException;
import com.dhwang.ytpokji.api.user.entity.User;
import com.dhwang.ytpokji.api.user.service.UserService;
import com.mysql.jdbc.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static com.dhwang.ytpokji.api.constant.Constant.User.API_METHOD;
import static com.dhwang.ytpokji.api.constant.Constant.User.API_USER;
import static com.dhwang.ytpokji.api.constant.Constant.User.CURRENT_USER;
import static com.dhwang.ytpokji.api.constant.Constant.User.YTPOKJI_TOKEN;

@Component
@Slf4j
public class CurrentUserFilter implements Filter {

    @Autowired
    private UserService userService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("-----log print:begin to call init method-----");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        if(isLoginUri(req) || isResourcesRequest(req)){
            chain.doFilter(request, response);
        }else{
            String token = req.getHeader(YTPOKJI_TOKEN);
            log.info("-----log print:begin to call doFilter method-----,and the token in request is {}", token);
            if (!StringUtils.isNullOrEmpty(token) && userExist(token)) {
                RequestContextHolder.getRequestAttributes().setAttribute(CURRENT_USER, token, RequestAttributes.SCOPE_REQUEST);
                chain.doFilter(request, response);
            } else {
                throw new BusinessException("Authentication failed!");
            }
        }
    }

    private boolean isResourcesRequest(HttpServletRequest request) {
        return !request.getRequestURI().contains("/api/");
    }

    private Boolean isLoginUri(HttpServletRequest req){
        return req.getRequestURI().equals(API_USER) && req.getMethod().equals(API_METHOD);
    }

    private boolean userExist(String token) {
        User user = userService.getUserById(token);
        return user != null;
    }

    @Override
    public void destroy() {
        log.info("-----log print:begin to call destroy method-----");
    }
}
