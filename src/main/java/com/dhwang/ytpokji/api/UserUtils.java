package com.dhwang.ytpokji.api;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import static com.dhwang.ytpokji.api.constant.Constant.User.CURRENT_USER;

public class UserUtils {

    private UserUtils(){

    }

    public static String getUserId(){
        return (String) RequestContextHolder.getRequestAttributes()
                .getAttribute(CURRENT_USER, RequestAttributes.SCOPE_REQUEST);
    }
}
