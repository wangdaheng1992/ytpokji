package com.dhwang.ytpokji.api.file.repository;

import com.dhwang.ytpokji.api.file.entity.BlogImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BlogImageRepository extends JpaRepository<BlogImage, String> {
}
