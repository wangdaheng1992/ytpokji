package com.dhwang.ytpokji.api.file.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "blog_image")
@Entity
public class BlogImage {

    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "blog_image_id_seq", sequenceName = "blog_image_id_seq")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    @Column(name = "length")
    private Long length;

    @Column(name = "url")
    private String url;

    public BlogImage(String name, String type, Long length, String url) {
        this.name = name;
        this.type = type;
        this.length = length;
        this.url = url;
    }
}
