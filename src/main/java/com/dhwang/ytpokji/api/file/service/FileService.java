package com.dhwang.ytpokji.api.file.service;

import com.dhwang.ytpokji.api.file.entity.BlogImage;
import com.dhwang.ytpokji.api.file.repository.BlogImageRepository;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.region.Region;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Date;
import java.util.UUID;

@Service
public class FileService {

    @Autowired
    private BlogImageRepository blogImageRepository;

    private static String AK = "AKIDVs43oLxiX7SYOzhQIuHOGtHROCm8mCsK";
    private static String SK = "MVP6ZoqeEbjfMr8BzZu1ua8TaS7XidKm";
    private static String REGION_NAME = "ap-guangzhou";
    private static String BUCKET_NAME = "dhwang-1256649202";
    private static String tempDirectory = "/aaa";

    public String upload(MultipartFile file) throws IOException {
        COSClient cosClient = getCosClient();
        String key = file.getOriginalFilename();
        File tempFile = new File(tempDirectory, key);
        FileUtils.writeByteArrayToFile(tempFile, file.getBytes());

        PutObjectRequest putObjectRequest = new PutObjectRequest(BUCKET_NAME, key, tempFile);
        cosClient.putObject(putObjectRequest);
        return getUploadUrl(cosClient, key, file.getSize());
    }

    private String getUploadUrl(COSClient cosClient, String key, long size) throws UnsupportedEncodingException {
        Date expiration = new Date(new Date().getTime() + 5 * 60 * 10000);
        URL url = cosClient.generatePresignedUrl(BUCKET_NAME, key, expiration);
        String path =  url.getProtocol() + "://" + url.getHost() + URLDecoder.decode(url.getPath(), "utf-8");
        return saveToDb(key, size ,path);
    }

    private String saveToDb(String fileName, long fileSize, String path){
        BlogImage image = new BlogImage(fileName, "jpg", fileSize, path);
        String id = UUID.randomUUID().toString();
        image.setId(id);
        blogImageRepository.save(image);
        return id;
    }

    private COSClient getCosClient() {
        COSCredentials cred = new BasicCOSCredentials(AK, SK);
        ClientConfig clientConfig = new ClientConfig(new Region(REGION_NAME));
        return new COSClient(cred, clientConfig);
    }
}
