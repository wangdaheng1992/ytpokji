package com.dhwang.ytpokji.api.file.controller;

import com.dhwang.ytpokji.api.file.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@RestController
public class FileUploadController {

    @Autowired
    private FileService fileService;

    @PostMapping("/api/file")
    public String upload(@RequestParam(value = "file", required = false) MultipartFile doc) throws IOException {
        return fileService.upload(doc);
    }

}
