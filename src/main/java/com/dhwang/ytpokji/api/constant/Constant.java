package com.dhwang.ytpokji.api.constant;

public class Constant {

    private Constant() {

    }

    public static class User {

        private User() {
        }

        public static final String YTPOKJI_TOKEN = "YTPOKJI-DH-TOEKN";

        public static final String CURRENT_USER = "CURRENT_USER";

        public static final String API_USER = "/api/users";

        public static final String API_METHOD = "POST";
    }
}
